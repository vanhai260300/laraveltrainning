<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserPostRequest;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index()
    {

        return view('admin.pages.register');
    }
    public function register(RegisterUserPostRequest $request){
        $request->validated();
        $res = User::addUser($request);
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

//use App\Http\Request;

class LoginController extends Controller
{
    //
    public function index()
    {
        return view('Admin.pages.login');
    }
    public function login(Request $request)
    {
//        echo "login:";
//        return redirect()->route("dashboard");
        $credentials = $request->validate([
            'email' => ['required','email'],
            'password' => ['required']
        ], [
            'email.require' => 'Email Require',
            'email.email' => 'Is Email',
            'password' => 'Password require'
        ]);
//        ['email' => $request->email,'password' => $request->password]
        if (Auth::attempt($credentials))
        {
//
            $request -> session() -> regenerate();
           return redirect()->intended('dashboard')->with( ['succses' => 1] );
        } else {
            return back()->withErrors(
                ['message' => 'Login information is incorrect, please check again']
            )->withInput();
        }

//        return view('Admin.pages.login');
    }
    public  function  logout(){
        session()->flush();
        Auth::logout();
        return redirect()->route("admin.login");
    }
}

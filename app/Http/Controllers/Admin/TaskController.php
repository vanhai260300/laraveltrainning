<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function index()
    {
        $listTask = Task::paginate (10);
        $data['listTask'] = $listTask;
        return view('admin.pages.task.task', $data);
    }
    public function addTask(){
        $this ->authorize('create', Task::class );
        return view('admin.pages.task.addtask');
    }
    public function editTask($idtask){
        $task = Task::find($idtask);
        $this->authorize('update',$task);
        $data['task'] = $task;
        return view('admin.pages.task.edittask',$data);
    }
}

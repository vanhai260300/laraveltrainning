<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\Product_category;
use App\Models\product_color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return |\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchString = trim($request->searchString);
        $filterby = $request->filterby;
        $products = Product::findProduct($filterby, $searchString);
        $data['searchString'] = $searchString;
        $data['filterby'] = $filterby;
        $data['products'] = $products;
        return view('admin.pages.product.products',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create',Product::class);
        $categories = Category::orderBy('name')->get();
        $colors = Color::orderBy('name')->get();
        $data['categories'] = $categories;
        $data['colors'] = $colors;
        return view('admin.pages.product.addproduct',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StorePostRequest $request)
    {
        $this->authorize('create',Product::class);
        $request -> validated();
        $result = Product::createProduct($request);
        if ($result == true)
            return redirect()->route('admin.product.index')->with( ['editsuccses' => $result] );
        else
            return redirect()->back()->with(['editsuccses' => $result]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::find($id);
        if($product){
            $this->authorize('update',$product);
            $categories = Category::orderBy('name')->get();
            $colors = Color::orderBy('name')->get();
            $data['colors'] = $colors;
            $data['categories'] = $categories;
            $data['product'] = $product;
            return view('admin.pages.product.editproduct',$data);
        }
        return redirect()->back()->with(['editsuccses' => -1]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $request -> validated();
        $product = Product::find($id);
        if ($product){
            $result = Product::updateProduct($request,$id);
            if ($result == true)
                return redirect()->route('admin.product.index')->with( ['editsuccses' => $result] );
            else
                return redirect()->back()->withErrors(['Edit new Product is Fail']);
        } else {

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response|int
     */
    public function destroy($idv, Request $request)
    {
//
        if ($request->checkdelete == 1){
            $id = $request->id;
            $product = Product::find($id);
            if($product){
                DB::beginTransaction();
                $this->authorize('delete',$product);
                $product->colors()->detach($product->colors);
                $productCategory =  Product_category::where('product_id',$id)->first();
                $productCategory -> delete();
                $product->delete();
                DB::commit();
                return 1;
            } else {
                return 0;
            }
        } else {
            $ids = $request->ids;
            Product_category::whereIn('product_id',explode(",", $ids))->delete();
            product_color::whereIn('product_id',explode(",", $ids))->delete();
            Product::whereIn('id',explode(",", $ids))->delete();
            return 1;
        }

    }
//    public function deleteMulti(Request $request){
//
//    }

}

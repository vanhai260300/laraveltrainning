<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameproduct' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'numeric|min:0',
            'description' => 'required',
        ];
    }
    public function messages(){
        return [
            'nameproduct.required' => 'Requires entering product name',
            'description.required' => 'Product description cannot be blank',
            'image.required' => 'You havent selected an image yet'
        ];
    }

}

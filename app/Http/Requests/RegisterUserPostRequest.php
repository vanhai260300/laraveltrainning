<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameuser' => 'required',
            'emailuser' => 'required|email|unique:users,email',
            'password' => 'required|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'nameuser.required' => 'Please enter Name',
            'emailuser.required' => 'Please enter Email',
            'emailuser.email' => 'Please enter Email',
            'emailuser.unique' => 'Email already exists',
            'password' => 'Please enter password'
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'users';
    public function role(){
        return $this->belongsTo(Role::class, 'is_role', 'id');
    }
    public function product(){
        return $this->hasMany(Product::class, 'is_user', 'id');
    }
    public function task(){
        return $this->hasMany(Task::class, 'is_user', 'id');
    }
    public static function getUser()
    {
        return [
            'name' => 'Nguyễn Văn Hải',
            'email' => 'Vanhai@gmail',
            'phone' => '123'
        ];
    }
    public static function addUser($request)
    {

        $users = new User;
        $users -> name = $request -> nameuser;
        $users -> email = $request -> emailuser;
        $users -> password = bcrypt($request -> password);
        $users -> created_at = now();
        $users -> is_role = 3;
        return $users -> save();
    }
    public static function editUser($request,$id){
        $users = User::find($id);
        $users -> name = $request -> nameuser;
        $users -> email = $request -> emailuser;
        $users -> updated_at = now();
        return $users -> save();
    }
    public static function deleteUser($id){
        $user = User::find($id);
        $user -> delete();
    }
//    protected $fillable = [
//        'name',
//        'email',
//        'password',
//    ];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}

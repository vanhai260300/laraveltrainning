<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;
    protected $table = "products";
    public function user(){
        return $this->belongsTo(User::class, 'is_user', 'id');
    }
    public function colors(){
//        return $this->hasMany(product_color::class, 'product_id', 'id');
//        return $this->hasManyThrough(product_color::class, Color::class,'id','id');
        return $this->belongsToMany(Color::class, product_color::class, 'product_id', 'color_id');
    }
    public function category(){
        return $this->hasOne(Product_category::class, 'product_id','id');
    }
    public static function findProduct($filterby, $keyword){
        $products = Product::with('colors');
        switch ($filterby)
        {
            case 1:
                $products = Product::with('colors')->where('name', 'LIKE', '%' . $keyword . '%');
                break;
            case 2:
                $products = Product::whereHas('colors', function ($query) use ($keyword) {
                    $query->where('name', 'LIKE', '%' . $keyword . '%');
                });
                break;
            case 3:
                $products = Product::with('colors')->where('description', 'LIKE', '%' . $keyword . '%');
                break;
        }
        $products = $products -> orderBy('id', 'desc')->paginate(10);
        return $products;
    }
    public static function createProduct($request){
        //Insertproduct
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('libs/images/products'), $imageName);
        try {
            DB::beginTransaction();
            $product = new Product();
            $product -> product_code = "MSP".$product -> id;
            $product -> name = $request->nameproduct;
            $product -> price = $request->price;
            $product -> image = $imageName;
            $product -> description = $request->description;
            $product -> created_at = now();
            $product ->save();
            //Add color for product
            $product->colors()->attach($request->colors);
            //Inssert Product_Category
            $productCategory =  new Product_category();
            $productCategory -> product_id = $product->id;
            $productCategory -> category_id = $request -> category;
            $productCategory -> save();
            DB::commit();
            return true;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }

    }
    public static function updateProduct($request, $id){
        $product = Product::find($id);
        if($request->image != ""){
            $file_path = public_path('libs/images/products/' . $product -> image);
            try {
                unlink($file_path);
            } catch (\Exception $e){

            }
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('libs/images/products'), $imageName);
            $product -> image = $imageName;
        }
        try {
            DB::beginTransaction();
            $product -> product_code = "MSP".$product -> id;
            $product -> name = $request->nameproducta;
            $product -> price = $request->price;
            $product -> description = $request->description;
            $product -> created_at = Carbon::now();
            $product ->save();
            //Remove old and add new color
            $product->colors()->detach($product->colors);
            $product->colors()->attach($request->colors);
//        Update Product Category
            $productCategory =  Product_category::where('product_id',$id)->first();
            if($productCategory){
                $productCategory -> category_id = $request -> category;
                $productCategory -> save();
            } else {
                $productCategory =  new Product_category();
                $productCategory -> product_id = $id;
                $productCategory -> category_id = $request -> category;
                $productCategory -> save();
            }
            DB::commit();
            return true;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }
}

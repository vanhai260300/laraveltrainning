@if ($paginator->hasPages())
    <div class="card-footer clearfix">
        <ul class="pager pagination pagination-sm m-0 float-right">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled"><span>« </span></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">« </a></li>
            @endif
{{--            {{dd($elements)}}--}}
            @foreach ($elements as $element)
{{--                @if (is_string($element))--}}
{{--                    <li class="disabled page-item"><span>{{ $element }}</span></li>--}}
{{--                @endif--}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active my-active page-item"><span>{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
                <li><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next"> »</a></li>
            @else
                <li class="disabled"><span> »</span></li>
            @endif
        </ul>
    </div>

@endif


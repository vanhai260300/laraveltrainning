<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<link rel="shortcut icon" type="image/x-icon" href="libs/images/favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900,900italic&amp;subset=latin,latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open%20Sans:300,400,400italic,600,600italic,700,700italic&amp;subset=latin,latin-ext" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/animate.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/chosen.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('libs/websitelte/css/color-01.css')}}">
</head>
<body class="home-page home-01 ">

	@include('website.layouts.header')

	<!--main area-->
	<main id="main" class="main-site left-sidebar">

	@yield('content')

	</main>
	<!--main area-->
    @include('website.layouts.footer')
    <script src="{{asset('libs/websitelte/js/jquery-1.12.4.minb8ff.js?ver=1.12.4')}}"></script>
	<script src="{{asset('libs/websitelte/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('libs/websitelte/js/chosen.jquery.min.js')}}"></script>
	<script src="{{asset('libs/websitelte/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('libs/websitelte/js/jquery.sticky.js')}}"></script>
	<script src="{{asset('libs/websitelte/js/functions.js')}}"></script>
</body>
</html>

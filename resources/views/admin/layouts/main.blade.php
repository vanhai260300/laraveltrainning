<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin | {{ $title ?? Dashboard }}</title>

    @include('admin.layouts.header-libs')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">


        @include('admin.layouts.header')
        @include('admin.layouts.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
            <div class="toastr-info">
                {{ Session::get('succses') }}
            </div>
        @include('admin.layouts.footer')
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->


    @include('admin.layouts.footer-libs')
    <script>
        @if ( Session::get('succses') == 1)
            toastr.options.timeOut = 3000; // How long the toast will display without user interaction
            toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
            toastr.info("Login Success","Success");
        @endif
    </script>
</body>

</html>

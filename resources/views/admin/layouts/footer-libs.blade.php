<!-- jQuery -->
<script src="{{asset('libs/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('libs/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('libs/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('libs/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('libs/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('libs/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('libs/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('libs/moment/moment.min.js')}}"></script>
<script src="{{asset('libs/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('libs/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('libs/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('libs/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
{{--data table--}}
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/datatables.min.js"></script>
<!-- AdminLTE App -->
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{asset('libs/adminlte/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('libs/adminlte/js/pages/dashboard.js')}}"></script>
<script src="{{asset('libs/adminlte/js/main.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
{{--CKEditor--}}
<script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
{{--<script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/inline/ckeditor.js"></script>--}}
<script type="text/javascript">
    function createCKEditor(){
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .catch( error => {
                console.error( error );
            } );
    }
</script>
@stack('custom-scripts')

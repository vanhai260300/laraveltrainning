@extends('admin.layouts.main',['title' => 'Users'])
@section('content')
    <div class="container">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List of Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="option-data">
                    <div class="my-2 left-option">
                        <input id="checkedAll" type="checkbox">
                        <buttom id="mutipDelete" class="btn btn-danger mx-1"><i class="fas fa-trash-alt"></i> Delete</buttom>
                        <a href="{{ route('admin.user.index') }}" class="btn btn-primary mx-1"><i class="fas fa-sync"></i> Refresh</a>
                        @can('create', \App\Models\User::class)
                        <a href="{{ route('admin.user.create') }}" class="btn btn-success mx-1"><i class="fas fa-user-plus"></i></a>
                        @endcan
                    </div>
                    <div class="right-option">
                        <form action="simple-results.html">
                            <div class="input-group text-center">
                                <label class="form-control form-control-md " for="filterby">Filter by: </label>
                                <select class="form-control form-control-md" id="filterby" name="filterby" id="filterby">
                                    <option value="1">ID</option>
                                    <option value="2">Name</option>\
                                    <option value="3">Email</option>
                                </select>
                                <input type="search" class="form-control form-control-md" placeholder="Search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-md btn-primary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Create At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listUser as $key => $value)
                        <tr id="row_{{ $value -> id  }}">
                            <td><input  type="checkbox" value="{{ $value -> id  }}" name="checkitems[]" id="check-item"></td>
                            <td>{{ $value -> id  }}</td>
                            <td>{{ $value -> name }}</td>
                            <td>{{ $value -> email }}</td>
                            <td>{{ $value -> created_at }}</td>
                            <td class="text-center">
                                <div>
                                    @can('update', $value)
                                    <a title="Edit" class="text-info" href="{{ route('admin.user.edit', $value -> id ) }}"><i class="far fa-edit"></i></a>

                                    <a title="Delete" class="text-danger" onclick="deleteUser({{ $value -> id }})" data-toggle="modal" data-target="#modal-default"><i class="fas fa-trash-alt"></i></a>
                                        <a title="Edit" class="text-info edituser" onclick="updateAdmin({{ $value -> id }})" id="button_{{ $value -> id }}" data-toggle="modal" data-target="#myModalUpdate">Ajax</a>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="number-pages">
                {{ $listUser->links('vendor.pagination.custom') }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">
                    <p>Are you sure to delete User</p>
                    <input type="hidden" id="idUser">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button onclick="apceptDelete()" type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="myModalUpdate">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Confirm Delete User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">
                    <form method="post" id="formUd" enctype="multipart/form-data" class="form-horizontal">
                        <div class="row form-group my-1">
                            <div class="col  col-md-3 "><label for="id-ud" class=" form-control-label">ID</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="id-ud" name="id-ud" readonly class="form-control"></div>
                        </div>
                        <div class="row form-group my-1">
                            <div class="col  col-md-3 "><label for="fullname-ud" class=" form-control-label">Full name</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="fullname-ud" name="fullname-ud" placeholder="Enter full name" class="form-control"><small class="text-danger" id="fullnameNN"></small></div>
                        </div>
                        <div class="row form-group my-1">
                            <div class="col  col-md-3"><label for="username-ud" class=" form-control-label">Email</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="username-ud" name="email-ud" placeholder="Enter Email" class="form-control"><small class="text-danger" id="usernameNN"></small></div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button onclick="saveUpdate()" type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                            <!-- <input href="" type="submit" class="btn btn-primary" id="bt-save-ud" name="bt-save-ud" value="Lưu"> -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@push('custom-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            // $('.edituser').click(function (e){
            //     console.log($(this).parent().find('td:nth-child(3)').text());
            // });

            @if ( Session::get('addsuccses') == 1)
                toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                toastr.info("Add new user is success","Success");
            @endif
            @if ( Session::get('editsuccses') == 1)
                toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                toastr.info("AEdit user is success","Success");
            @endif
        });

        function updateAdmin(id){
            var formud = $('#formUd');
            formud.find('input[name="id-ud"]').val(id);
            formud.find('input[name="fullname-ud"]').val($('#row_'+id+' td:nth-child(3)').text());
            formud.find('input[name="email-ud"]').val($('#row_'+id+' td:nth-child(4)').text());
        }
        function saveUpdate(){
            var formud = $('#formUd');
            let idus = formud.find('input[name="id-ud"]').val();
            let fullname = formud.find('input[name="fullname-ud"]').val();
            let email = formud.find('input[name="email-ud"]').val();
            $.ajax({
                url: '{{ route('admin.user.update.ajax') }}',
                method: 'POST',
                data: {
                    idus: idus,
                    nameuser: fullname,
                    emailuser: email,
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    console.log(result);
                    if ( result == 1 ){
                        $('#row_'+idus+' td:nth-child(3)').text(fullname);
                        $('#row_'+idus+' td:nth-child(4)').text(email);
                        toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                        toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                        toastr.info("Update user is success","Success");
                        // $("#row_" + id).remove();
                    }
                }
            });
        }
        function deleteUser(id)
        {
            $("#idUser").val(id);
        }
        function apceptDelete(){
            let id = $("#idUser").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'DELETE',
                url: "user/"+id,
                data: { id: id , _token: '{{csrf_token()}}'},
                success:function(data){
                    // alert(data);
                    toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                    toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                    toastr.info("Delete user is success","Success");
                    $("#row_" + id).remove();
                }
            });


        }
    </script>
@endpush
@endsection

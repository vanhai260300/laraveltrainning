@extends('admin.layouts.main',['title' => 'Add New User'])
@section('content')
    <div class="container">
        <div class="card card-default my-2">
            <div class="card-header">
                <h3 class="card-title">Add New User</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('admin.user.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="error-content">
                        <ul>
                            @if ($errors -> any())
                                {{--                    {{ dd($errors) }}--}}
                                @foreach($errors->all() as $key => $value)
                                    <li class="text-danger">{{ $value }}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nameuser">Name</label>
                        <input type="text" class="form-control" name="nameuser" id="nameuser" placeholder="Enter Name" value="{{ old('nameuser') }}">
                    </div>
                    <div class="form-group">
                        <label for="emailuser">Email address</label>
                        <input type="text" class="form-control" id="emailuser" name="emailuser" value="{{ old('emailuser') }}" placeholder="Enter Email">
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password_confirmation">Repeat Password</label>
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Repeat Password">
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">

                    <a href="{{ route('admin.user.index') }}" class="btn btn-danger">Cancel</a>
                    <button type="submit"  class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection

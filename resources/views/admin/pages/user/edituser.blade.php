@extends('admin.layouts.main',['title' => 'Edit User'])
@section('content')
    <div class="container">
        <div class="card card-default my-2">
            <div class="card-header">
                <h3 class="card-title">Edit User <small>{{ $user->name }}</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('admin.user.update', $user->id)  }}" method="post">
                {{ method_field('PUT') }}}
                <div class="card-body">
                    <div class="error-content">
                        <ul>
                            @if ($errors -> any())
                                @foreach($errors->all() as $key => $value)
                                    <li class="text-danger">{{ $value }}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nameuser">Name</label>
                        <input type="text" class="form-control" name="nameuser" id="nameuser" placeholder="Enter Name" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label for="emailuser">Email address</label>
                        <input type="text" class="form-control" id="emailuser" name="emailuser" value="{{ $user->email }}" placeholder="Enter Email">
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">

                    <a href="{{ route('admin.user.index') }}" class="btn btn-danger">Cancel</a>
                    <button type="submit"  class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection
@push('custom-scripts')
    <script type="text/javascript">
{{--        @if ( Session::get('editsuccses') == -1)--}}
{{--            console.log("fail");--}}
{{--            toastr.options.timeOut = 3000; // How long the toast will display without user interaction--}}
{{--        toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it--}}
{{--        toastr.error("Edit new Product is Fail","Fail");--}}
{{--        @endif--}}
    </script>

@endpush

@extends('admin.layouts.main',['title' => 'Products'])
@section('content')


    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Product</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">

        <div class="card-header">
            <h3 class="card-title">Bordered Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="option-data">
                <div class="my-2 left-option">
                    <input id="checkedAll" type="checkbox">
                    @can('viewAny', \App\Models\Product::class)
                    <buttom id="" onclick="NotiDeleteMuti()" class="btn btn-danger mx-1"><i class="fas fa-trash-alt"></i> Delete</buttom>
                    @endcan
                    <a href="{{ route('admin.product.index') }}" class="btn btn-primary mx-1"><i class="fas fa-sync"></i> Refresh</a>
                    @can('create', \App\Models\Product::class)
                    <a href="{{ route('admin.product.create') }}" class="btn btn-success mx-1"><i class="fas fa-plus"></i></a>
                    @endcan
                </div>
                <div class="right-option">
                    <form action="{{ route('admin.product.index') }}" method="get">
                        <div class="input-group text-center">
                            <label class="form-control form-control-md " for="filterby">Filter by: </label>
                            <select class="form-control form-control-md" id="filterby" name="filterby" id="filterby">
                                <option value="1" {{ $filterby == 1 ? 'selected' : '' }}>Name</option>
                                <option value="2" {{ $filterby == 2 ? 'selected' : '' }}>Color</option>\
                                <option value="3" {{ $filterby == 3 ? 'selected' : '' }}>Description</option>
                            </select>
                            <input type="search" class="form-control form-control-md" onkeyup="findProduct()" value="{{ $searchString ? $searchString : '' }}" name="searchString" placeholder="Search" style="width: 30%">
                            <div class="input-group-append">
                                <button type="submit"  class="btn btn-md btn-primary">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
            <table class="table table-bordered" id="datatable-result">
                <thead>
                <tr>
                    <th></th>
                    <th>#</th>
                    <th style="width: 10px">ID</th>
                    <th>Name Product</th>
                    <th>Price</th>
                    <th>Igame</th>
                    <th>Description</th>
                    <th>Create At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $key => $value)
                <tr id="row_{{ $value -> id  }}">
                    <td><input  type="checkbox" value="{{ $value -> id  }}" name="checkitems[]" id="check-item"></td>
                    <td>
                        @if(count($value->colors) > 0)
                        <a class="text-primary" onclick="handleDetail({{ $value -> id  }})"><i class="fas fa-plus"></i> <i class="fas fa-minus text-danger"></i></a>
                        @endif
                    </td>
                    <td>{{ $value -> id  }}</td>
                    <td>{{ $value -> name }}</td>
                    <td>{{ $value -> price }}</td>
                    <td>
                        <img src="{{ asset('libs/images/products/'.$value -> image.'')}}" alt="" style="width: 60px;">

                    </td>
                    <td>{{ $value -> description }}</td>
                    <td>{{ $value -> created_at }}</td>
                    <td class="text-center">
                        <div>
                            @can('update', $value)
                            <a title="Edit" class="text-info" href="{{ route('admin.product.edit',$value)  }}"><i class="far fa-edit"></i></a>
                            @endcan
                            @can('delete', $value)
{{--                                    <form action="{{ route('admin.product.destroy', $value) }}" method="post">--}}
{{--                                        <input type="hidden" name="_method" value="DELETE">--}}
{{--                                        @method('DELETE')--}}
{{--                                        @csrf--}}
{{--                                        <button type="submit" title="Delete" class="text-danger"><i class="fas fa-trash-alt"></i></button>--}}
{{--                                    </form>--}}
{{--                                    <a title="Delete" href="{{ route('admin.product.destroy', $value) }}" class="text-danger"><i class="fas fa-trash-alt"></i></a>--}}

                                    <a title="Delete" class="text-danger" onclick="deleteProduct({{ $value -> id }})"><i class="fas fa-trash-alt"></i></a>
                            @endcan
                        </div>
                    </td>
                </tr>
                @if(count($value->colors) > 0)
                <tr class="row-detail-hide bg-light row-detail_{{ $value -> id  }}">
                    <td colspan="3">Color: </td>
                    <td colspan="5">
                        <div class="">
                            <ol class="m-0">
                                @foreach( $value->colors as $colorr)
                                    <li class="mx-3" style="color: {{$colorr->name}}"> {{ $colorr->name }} </li>
                                @endforeach
                            </ol>

                        </div>
                    </td>

                </tr>
                @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="number-pages card-footer clearfix" >
            {{ $products->links() }}
        </div>

    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            $( document ).ready(function() {
                @if ( Session::get('addsuccses') == 1)
                    toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                toastr.info("Add new Product is success","Success");
                @endif

                    @if ( Session::get('editsuccses') == 1)
                    toastr.options.timeOut = 3000; // How long the toast will display without user interaction
                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it
                toastr.info("Edit Product is success","Success");
                @endif

            });
            function handleDetail(id){
                // $(".row-detail-hide").hide();
                $("#row_"+id+ " .fa-minus").toggle();
                $("#row_"+id+ " .fa-plus").toggle();
                $(".row-detail_"+id).toggle();
            }
            function deleteProduct(id){
                Swal.fire({
                    icon: 'warning',
                    title: 'Are you sure?',
                    text: 'Delete this product!',
                    showCancelButton: true,
                    confirmButtonText: '<a style = "Color:White;" onclick="deleteRow(' + id + ')">Delete</a>',
                    cancelButtonText: 'Cancel'
                });
            }
            function deleteRow(id){
                $.ajax({
                    url: 'product/'+id,
                    method: 'DELETE',
                    data: {
                        id: id,
                        checkdelete: 1,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        console.log(result);
                        $("#row_" + id).remove();
                        $(".row-detail_" + id).remove();
                        NotificationSuccess();
                    }
                });
            }
            function NotiDeleteMuti(){
                items = $('input[name="checkitems[]"]:checked');
                if (items.length > 0){
                    Swal.fire({
                        icon: 'warning',
                        title: 'Are you sure?',
                        text: 'Delete this products!',
                        showCancelButton: true,
                        confirmButtonText: '<a style = "Color:White;" onclick="DeleteMuti()">Delete</a>',
                        cancelButtonText: 'Cancel'
                    });
                }

            }
            function NotificationSuccess(){
                Swal.fire({
                    icon: 'success',
                    title: 'Delete Success?',
                    showCancelButton: true,
                    confirmButtonText: 'OKay',
                });
            }
            function DeleteMuti(){
                items = $('input[name="checkitems[]"]:checked');
                var allVals = [];
                for (let item of items)
                {
                    allVals.push(item.value);
                }
                join_selected_values = allVals.join(",")
                $.ajax({
                    // url: 'deletemulti',
                    url: 'product/1',
                    method: 'DELETE',
                    data: {
                        ids: join_selected_values,
                        checkdelete: 2,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        console.log(result);
                        if (result > 0){
                            console.log(result);
                            allVals.forEach(function(e) {
                                $("#row_" + e).remove();
                                $(".row-detail_" + e).remove();
                            });
                            NotificationSuccess();
                        }


                    }
                });
            }
            function findProduct(){

                // $("#datatable-result").DataTable(
                //     {
                //
                //     }
                // );
            }
        </script>
    @endpush
@endsection

@extends('admin.layouts.main',['title' => 'Add New Product'])
@section('content')
    <div class="container">
        <div class="card card-default my-2">
            <div class="card-header">
                <h3 class="card-title">Add New Product</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="error-content">
                        <ul>
                            @if ($errors -> any())
                                {{--                    {{ dd($errors) }}--}}
                                @foreach($errors->all() as $key => $value)
                                    <li class="text-danger">{{ $value }}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="nameproduct">Name product</label>
                        <input type="text" class="form-control" name="nameproduct" id="nameproduct" placeholder="Enter Name product" value="{{ old('nameproduct') }}">
                    </div>
                    <div class="form-group ">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}" placeholder="Enter price">
                    </div>
                    <div class="form-group ">
                        <label for="category">Category</label>
                        <select name="category" id="category" class="form-control">
                            <option value="" disabled selected>Select category</option>
                        @foreach($categories as $key => $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group ">
                        <label for="colors">Colors</label>
                        <select name="colors[]" id="colors" class="multiple-colors form-control" multiple="multiple">
                            @foreach($colors as $key => $color)
                                <option value="{{ $color->id }}">{{ $color->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 center upload-image">
                            <div class="btn-container">
                                <div class="imgupload">
                                    <div>
                                        <img class="imgPreview" src="{{asset('libs/images/products/imagefile.png')}}" alt="">
                                        <i class="fa fa-times removeImage"></i>
                                    </div>
                                </div>
                                <p id="namefile">Only pics allowed! (jpg,jpeg,bmp,png)</p>
                                <input type="file" class="form-control hidden" id="image" name="image" placeholder="Enter image">
                                <label type="button" id="btnup" for="image" class="btn btn-primary">Browse for your pic!</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="description" name="description" rows="4">{{ old('description') }}</textarea>
                    </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <a href="{{ route('admin.product.index') }}" class="btn btn-danger">Cancel</a>
                    <button type="submit"  class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            createCKEditor();
            $(".multiple-colors").select2({
                maximumSelectionLength: 5
            });
        </script>
    @endpush
@endsection


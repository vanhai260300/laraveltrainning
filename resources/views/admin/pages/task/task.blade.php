@extends('admin.layouts.main',['title' => 'Task'])
@section('content')
    <div class="container">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Task</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List of Task</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="option-data">
                    <div class="my-2 left-option">
                        @can('viewAny', \App\Models\Task::class)
                        <input id="checkedAll" type="checkbox">
                        <buttom id="mutipDelete" class="btn btn-danger mx-1"><i class="fas fa-trash-alt"></i> Delete</buttom>
                        @endcan
                        <a href="{{ route('admin.list-task') }}" class="btn btn-primary mx-1"><i class="fas fa-sync"></i> Refresh</a>
                        @can('create', \App\Models\Task::class)
                        <a href="{{ route('admin.addtask') }}" class="btn btn-success mx-1"><i class="fas fa-plus"></i></a>
                        @endcan
                    </div>
                    <div class="right-option">
                        <form action="simple-results.html">
                            <div class="input-group text-center">
                                <label class="form-control form-control-md " for="filterby">Filter by: </label>
                                <select class="form-control form-control-md" id="filterby" name="filterby" id="filterby">
                                    <option value="1">ID</option>
                                    <option value="2">Name</option>
                                    <option value="3">Description</option>
                                    <option value="4">Status</option>

                                </select>
                                <input type="search" class="form-control form-control-md" placeholder="Search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-md btn-primary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        @can('viewAny', \App\Models\Task::class)
                        <th>#</th>
                        @endcan
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listTask as $key => $value)
                        <tr id="row_{{ $value -> id  }}">
                            @can('viewAny', \App\Models\Task::class)
                            <td><input  type="checkbox" value="{{ $value -> id  }}" name="checkitems[]" id="check-item"></td>
                            @endcan
                            <td>{{ $value -> id  }}</td>
                            <td>{{ $value -> name }}</td>
                            <td>{{ $value -> description }}</td>
                            <td>{{ $value -> status }}</td>
                            <td class="text-center">
                                <div>
                                    @can('update', $value)
                                    <a title="Edit" class="text-info" href="{{ route('admin.edittask', $value -> id) }}"><i class="far fa-edit"></i></a>
                                    @endcan
                                    @can('delete', $value)
                                            <a title="Delete" class="text-danger" onclick="deleteUser({{ $value -> id }})" data-toggle="modal" data-target="#modal-default"><i class="fas fa-trash-alt"></i></a>
                                        @endcan
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="number-pages">
                {{ $listTask->links('vendor.pagination.custom') }}
            </div>
        </div>
    </div>
    @push('custom-scripts')
{{--        <script type="text/javascript">--}}
{{--            $( document ).ready(function() {--}}
{{--                @if ( Session::get('addsuccses') == 1)--}}
{{--                    toastr.options.timeOut = 3000; // How long the toast will display without user interaction--}}
{{--                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it--}}
{{--                toastr.info("Add new user is success","Success");--}}
{{--                @endif--}}
{{--                    @if ( Session::get('editsuccses') == 1)--}}
{{--                    toastr.options.timeOut = 3000; // How long the toast will display without user interaction--}}
{{--                toastr.options.extendedTimeOut = 3000; // How long the toast will display after a user hovers over it--}}
{{--                toastr.info("AEdit user is success","Success");--}}
{{--                @endif--}}
{{--            });--}}

{{--        </script>--}}
    @endpush
@endsection

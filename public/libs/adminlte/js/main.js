$(document).ready(function () {
    var checkAll = $("#checkedAll");
    var checkItem = $('input[name="checkitems[]"]');
    let btMutiDelete = $("#mutipDelete");
    checkAll.change(function () {
        var isCheckAll = $(this).prop('checked');
        checkItem.prop('checked', isCheckAll);
        // renderCheckBT();
    });
    checkItem.change(function () {
        var isCheckAll = checkItem.length === $('input[name="checkitems[]"]:checked').length;
        checkAll.prop('checked', isCheckAll);
        // console.log(isCheckAll);
        // renderCheckBT();
    });
    btMutiDelete.click(function (){
        items = $('input[name="checkitems[]"]:checked');
        var allVals = [];
        for (let item of items)
        {
            allVals.push(item.value);
            $("#row_" + item.value).remove();
        }

    })
    // function renderCheckBT() {
    //     count = $('input[name="checkitems[]"]:checked').length;
    //     if (count > 0) {
    //         btnCheckSubmit.removeClass("disabled");
    //         btnCheckSubmitBack.removeClass("disabled");
    //     } else {
    //         btnCheckSubmit.addClass("disabled");
    //         btnCheckSubmitBack.addClass("disabled");
    //     }
    // }
});


$("#image").change(function (event) {
    console.log('oke');
    FilePreview(this);
});
function FilePreview(input){
    if (input.files && input.files[0]) {
        if (!checkImage(input.files[0].name)) {
            // $("#namefile").show();
        } else {
            // $(".checkImage").hide();
            var reader = new FileReader();
            var filename = $("#image").val().split('\\').pop();
            $("#namefile").text(filename);
            reader.onload = function (e) {
                $('.imgPreview').attr('src', e.target.result);
                $('.removeImage').show();
            }
            reader.readAsDataURL(input.files[0]);

        }

    }
}
$('.removeImage').click(function (){
    $("#namefile").text('Choose a image.');
    $('.removeImage').hide();
    $("#image").val("");
    $('.imgPreview').attr('src', '/libs/images/products/imagefile.png');
});
function checkImage(url) {
    return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
}

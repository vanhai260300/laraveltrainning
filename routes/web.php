<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('website.login');
});
Route::get('/qrcode', function () {
    return view('welcome');
});
Route::get('/login','Website\LoginController@index' )->name('website.login');
Route::middleware('auth')->group( function (){
    Route::get('/dashboard',[ App\Http\Controllers\Admin\DashboardController::class , 'index'] )->name('dashboard');
    Route::get('/acc/logout', [ App\Http\Controllers\Admin\Auth\LoginController::class , 'logout'])->name('logout');
    Route::group([
        'as' => 'admin.',
        'prefix' => 'admin',
        'middleware' => 'is_admin'
    ], function (){
        Route::resource('/product',Admin\ProductController::class);
        Route::delete('/product-deletemulti',[\App\Http\Controllers\Admin\ProductController::class,'deleteMulti'])->name('product.deletemuti');
        Route::get('/product-search',[\App\Http\Controllers\Admin\ProductController::class,'searchProduct'])->name('product.search');
        Route::resource('/user', Admin\UserController::class);
        Route::post('/updateus-ajax', [\App\Http\Controllers\Admin\UserController::class, 'updateAjax'])->name('user.update.ajax');
        Route::get('/task',[ App\Http\Controllers\Admin\TaskController::class , 'index'] )->name('list-task');
        Route::get('/add-task',[ App\Http\Controllers\Admin\TaskController::class , 'addTask'] )->name('addtask');
        Route::get('/edit-task/{idtask}',[ App\Http\Controllers\Admin\TaskController::class , 'editTask'] )->name('edittask');
    });
}
);

Route::get('/admin/login',[\App\Http\Controllers\Admin\Auth\LoginController::class, 'index'] )->name('admin.login');
Route::post('/admin/login',[\App\Http\Controllers\Admin\Auth\LoginController::class, 'login'])->name('admin.login');
Route::get('/admin/register-user', [\App\Http\Controllers\Admin\Auth\RegisterController::class, 'index'] )->name('admin.user.register');
Route::post('/admin/register-user', [\App\Http\Controllers\Admin\Auth\RegisterController::class, 'register'] )->name('admin.user.register');

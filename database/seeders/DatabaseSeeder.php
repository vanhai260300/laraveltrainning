<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\OrderStatusSeeder;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Product_category;
use App\Models\Product_color;
use App\Models\Color;
use App\Models\Admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::insert( [
            [
                'name' => "ADMIN",
            ],
            [
                'name' => "MANAGER",
            ],
            [
                'name' => "USER",
            ] ]
        );
        User::insert(
            [
                'name' => "Admin",
                'email' => 'admin@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('admin'), // password
                'remember_token' => Str::random(10),
                'is_role' => 1

            ]
        );
        Admin::insert(
            [
                'name' => "Admin",
                'email' => 'admin@gmail.com',
                'password' => bcrypt('admin'), // password
                'remember_token' => Str::random(10),

            ]
        );
        Product::factory(100)->create();
        User::factory(20)->create();
        Category::factory(20)->create();
        Color::factory(20)->create();
        Product_category::factory(100)->create();
        Product_color::factory(100)->create();

            //     [
        // DB::table('products')->insert([
        //     'name' => 'Nguyễn Văn Hải',
        //     'price' => 200000,
        //     'description' => 'Tên đẹp nhỉ',
        //     'image' => 'person1'
        // ]);

    }
}

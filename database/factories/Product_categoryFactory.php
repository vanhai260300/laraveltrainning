<?php

namespace Database\Factories;

use App\Models\Product_category;
use Illuminate\Database\Eloquent\Factories\Factory;

class Product_categoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product_category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $number = 1;
        return [
            'product_id' => $number++,
            'category_id' => rand(1,20),
        ];
    }
}

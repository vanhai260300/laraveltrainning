<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $number = 1;
        return [
            'name' => $this->faker->name(),
            'price' => rand(10000,99999),
            'description' => $this->faker->sentence(20),
            'created_at' => now(),
            'product_code' => 'CPR'.$number++,
            'image' => '1633678738.png',
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\product_color;
use Illuminate\Database\Eloquent\Factories\Factory;

class product_colorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = product_color::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => rand(1,100),
            'color_id' => rand(1,20),
        ];
    }
}

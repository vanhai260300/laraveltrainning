<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence($nb = 3, $asText = false),
            'description' => $this->faker->sentence($nb = 20, $asText = false),
            'status' =>  rand(0,1),
            'id_user' => rand(1,20),
            'created_at' => now()
        ];
    }
}
